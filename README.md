# Lab5 -- Integration testing

## Introduction

We've covered unit testing, which is widely used in indusry and is completely whitebox, but there are times when you need to use blackbox testing, for example if you cannot access code module, or if you need to check propper work of module, while calling it locally, so it returns correct values. That's what integration testing is being used for, it can be used both for whitebox and blackbox testing, depending on your goals and possibilities. **_Let's roll!_**🚀️

## Integration testing

Well, if unit testing is all about atomicity and independency, integration testing doesn't think like it, it is used less often then unit testing because it requires more resourses, and I haven't met a team yet, where developers were doing integration tests, usually it is a task for the automated testing team, to check integration of one module with another. Main goal of integration testing is to check that two connected modules(or more, but usually two is enough, because it is less complicated to code and maintain)are working propperly.

## BVA

BVA is a top method for creating integration tests and blackbox tests, when using BVA you should take module(or few modules) and analyse inputs and theirs boudaries, and then pick values that are placed on the border of equivalence classes. To assure the correctness it is usually to chek one value in between of the boundaries just in case.

## Solution

Email: r.talalaeva@innopolis.university
Key: `AKfycby4bekTEgvzyqqCKaK8Fd5DO_R-6UIA7IIIpwq59iSOniYZxnV9VNYwK4q84et1j9B38w`

### Specs

```
Here is InnoCar Specs:
Budet car price per minute = 20
Luxury car price per minute = 42
Fixed price per km = 13
Allowed deviations in % = 19
Inno discount in % = 17
```

### BVA table

| Parameter        | Equivalence Classes           |
| ---------------- | ----------------------------- |
| type             | budget, luxury, nonsense      |
| plan             | fixed_price, minute, nonsense |
| distance         | <=0, >0, nonsense             |
| time             | <=0, >0, nonsense             |
| planned_distance | <=0, >0, nonsense             |
| planned_time     | <=0, >0, nonsense             |
| inno_discount    | yes, no, nonsense             |

### Decision Table

| Conditions (inputs) | Values                        | R1  | R2       | R3  | R4       | R5  | R6       | R7  | R8       | R9       | R10      | R11      | R12         | R13    | R14    | R15    | R16    | R13         | R14         |
| ------------------- | ----------------------------- | --- | -------- | --- | -------- | --- | -------- | --- | -------- | -------- | -------- | -------- | ----------- | ------ | ------ | ------ | ------ | ----------- | ----------- |
| type                | budget, luxury, nonsense      | \*  | \*       | \*  | \*       | \*  | \*       | \*  | \*       | nonsense | \*       | \*       | luxury      | budget | budget | luxury | luxury | budget      | budget      |
| plan                | minute, fixed_price, nonsense | \*  | \*       | \*  | \*       | \*  | \*       | \*  | \*       | \*       | nonsense | \*       | fixed_price | minute | minute | minute | minute | fixed_price | fixed_price |
| distance            | <=0, >0, nonsense             | <=0 | nonsense | >0  | >0       | >0  | >0       | >0  | >0       | >0       | >0       | >0       | >0          | >0     | >0     | >0     | >0     | >0          | >0          |
| time                | <=0, >0, nonsense             | \*  | \*       | \*  | \*       | <=0 | nonsense | >0  | >0       | >0       | >0       | >0       | >0          | >0     | >0     | >0     | >0     | >0          | >0          |
| planned_distance    | <=0, >0, nonsense             | \*  | \*       | <=0 | nonsense | >0  | >0       | >0  | >0       | >0       | >0       | >0       | >0          | >0     | >0     | >0     | >0     | >0          | >0          |
| planned_time        | <=0, >0, nonsense             | \*  | \*       | \*  | \*       | \*  | \*       | <=0 | nonsense | >0       | >0       | >0       | >0          | >0     | >0     | >0     | >0     | >0          | >0          |
| inno_discount       | yes, no, nonsense             | \*  | \*       | \*  | \*       | \*  | \*       | \*  | \*       | \*       | \*       | nonsense | \*          | yes    | no     | no     | yes    | yes         | no          |
| 200                 | 200                           |     |          |     |          |     |          |     |          |          |          |          |             | X      | X      | X      | X      | X           | X           |
| Invalid Request     | Invalid Request               | X   | X        | X   | X        | X   | X        | X   | X        | X        | X        | X        | X           |        |        |        |        |             |             |

### Test Cases

| ID  | Desition Table Entry | distance | planned_distance | time     | planned_time | type     | plan        | inno_discount | Expected Result | Actual Result               |
| --- | -------------------- | -------- | ---------------- | -------- | ------------ | -------- | ----------- | ------------- | --------------- | --------------------------- |
| 0   | R1                   | -1       | 1                | 1        | 1            | budget   | fixed_price | yes           | Invalid Request | Invalid Request             |
| 1   | R1                   | 0        | 1                | 1        | 1            | budget   | fixed_price | yes           | Invalid Request | "price": 13.000000000000002 |
| 2   | R2                   | nonsense | 1                | 1        | 1            | budget   | fixed_price | yes           | Invalid Request | "price": 13.000000000000002 |
| 3   | R3                   | 1        | -1               | 1        | 1            | budget   | fixed_price | yes           | Invalid Request | Invalid Request             |
| 4   | R3                   | 1        | 0                | 1        | 1            | budget   | fixed_price | yes           | Invalid Request | "price": 13.000000000000002 |
| 5   | R4                   | 1        | nonsense         | 1        | 1            | budget   | fixed_price | yes           | Invalid Request | "price": 13.000000000000002 |
| 6   | R5                   | 1        | 1                | -1       | 1            | budget   | fixed_price | yes           | Invalid Request | Invalid Request             |
| 7   | R5                   | 1        | 1                | 0        | 1            | budget   | fixed_price | yes           | Invalid Request | "price": 0                  |
| 8   | R6                   | 1        | 1                | nonsense | 1            | budget   | fixed_price | yes           | Invalid Request | "price": null               |
| 9   | R7                   | 1        | 1                | 1        | -1           | budget   | fixed_price | yes           | Invalid Request | Invalid Request             |
| 10  | R7                   | 1        | 1                | 1        | 0            | budget   | fixed_price | yes           | Invalid Request | "price": 13.000000000000002 |
| 11  | R8                   | 1        | 1                | 1        | nonsense     | budget   | fixed_price | yes           | Invalid Request | "price": 13.000000000000002 |
| 12  | R9                   | 1        | 1                | 1        | 1            | nonsense | minute      | yes           | Invalid Request | Invalid Request             |
| 13  | R10                  | 1        | 1                | 1        | 1            | budget   | nonsense    | yes           | Invalid Request | Invalid Request             |
| 14  | R11                  | 1        | 1                | 1        | 1            | budget   | minute      | nonsense      | Invalid Request | Invalid Request             |
| 15  | R12                  | 1        | 1                | 1        | 1            | luxury   | fixed_price | yes           | Invalid Request | Invalid Request             |
| 16  | R13                  | 1        | 1                | 1        | 1            | budget   | minute      | yes           | "price":20.28   | "price":20.28               |
| 17  | R14                  | 1        | 1                | 1        | 1            | budget   | minute      | no            | "price":26      | "price":26                  |
| 18  | R15                  | 1        | 1                | 1        | 1            | luxury   | minute      | no            | "price":29.4    | "price":29.4                |
| 19  | R16                  | 1        | 1                | 1        | 1            | luxury   | minute      | yes           | "price":22.932  | "price":22.932              |

### Bugs:

ID1 - `0` allowed in distance

ID2 - `nonsense` allowed in distance

ID4 - `0` allowed in planned distance

ID5 - `nonsense` allowed in planned distance

ID7 - `0` allowed in time

ID8 - `nonsense` allowed in time

ID10 - `0` allowed in planned time

ID11 - `nonsense` allowed in planned time
